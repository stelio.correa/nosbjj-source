<?php

use Illuminate\Database\Seeder;

class KinshipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'description' => 'PAI'
            ],
            [
                'description' => 'MÃE'
            ],
            [
                'description' => 'TIO'
            ],
            [
                'description' => 'TIA'
            ],
            [
                'description' => 'AVÔ'
            ],
            [
                'description' => 'AVÓ'
            ]
        ];

        DB::table('kinships')->insert($data);

    }
}
