<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => Webpatser\Uuid\Uuid::generate(),
                'name' => 'ADMINISTRADOR DO SISTEMA',
                'cpf' => '00000000000',
                'email' => 'contato@nosbjj.com.br',
                'password' => bcrypt('#Adm1n'),
                'active' => true,
                'role' => 'ADMINISTRATOR'
            ]
        ];

        DB::table('users')->insert($data);

    }
}
