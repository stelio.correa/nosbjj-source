<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaritalsStatusTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'description' => 'SOLTEIRO(A)'
            ],
            [
                'description' => 'CASADO(A)'
            ],
            [
                'description' => 'DIVORCIADO(A)'
            ],
            [
                'description' => 'VIUVO(A)'
            ],
            [
                'description' => 'UNIÃO ESTÁVEL'
            ]
        ];

        DB::table('maritals_status')->insert($data);


    }
}
