<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorsTable extends Migration
{
    public function up()
    {
        Schema::create('sponsors', function (Blueprint $table) {
            $table->uuid('id')
                ->primary('id');
            $table->string('name', 100);
            $table->string('phone', 20)->nullable();
            $table->string('cellphone', 20)->nullable();
            $table->string('cep', 8)->nullable();
            $table->string('address', 50)->nullable();
            $table->string('number', 15)->nullable();
            $table->string('address_complement', 50)->nullable();
            $table->string('neighborhood', 25)->nullable();
            $table->string('city', 35)->nullable();
            $table->string('country', 2)->nullable();
            $table->timestamps();
        });

        Schema::table('sponsors', function (Blueprint $table) {
            $table->string('registration_id',36);
            $table->integer('kinship_id')->unsigned();

            $table->foreign('registration_id')
                ->references('id')->on('registrations');

            $table->foreign('kinship_id')
                ->references('id')->on('kinships');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sponsors');
    }
}
