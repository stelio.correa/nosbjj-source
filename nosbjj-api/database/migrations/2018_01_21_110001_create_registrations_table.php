<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->uuid('id')
                ->primary('id');
            $table->integer('registration_number')->unique();
            $table->string('name', 100);
            $table->char('gender', 1)->default('M');
            $table->date('date_of_birth')->nullable();
            $table->string('cpf', 11)->unique();
            $table->string('profession', 100);
            $table->string('degree_education', 100);
            $table->string('email', 100)->unique();
            $table->string('rg_number', 15)->nullable();
            $table->string('rg_emitter', 25)->nullable();
            $table->string('phone', 15)->nullable();
            $table->string('cellphone', 15)->nullable();
            $table->string('cep', 8)->nullable();
            $table->string('address', 50)->nullable();
            $table->string('number', 15)->nullable();
            $table->string('address_complement', 50)->nullable();
            $table->string('neighborhood', 25)->nullable();
            $table->string('city', 35)->nullable();
            $table->string('country', 2)->nullable();

            $table->timestamps();
        });

        Schema::table('registrations', function (Blueprint $table) {
            $table->string('user_id', 36);
            $table->integer('marital_status_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')->on('users');

            $table->foreign('marital_status_id')
                ->references('id')->on('maritals_status');
        });
    }

    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
