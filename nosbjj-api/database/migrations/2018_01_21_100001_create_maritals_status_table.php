<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaritalsStatusTable extends Migration
{
    public function up()
    {
        Schema::create('maritals_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 35);
        });
    }

    public function down()
    {
        Schema::dropIfExists('maritals_status');
    }
}
