<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')
                ->primary('id');
            $table->string('name', 100);
            $table->string('cpf', 11)->unique();
            $table->string('cellphone', 15)->nullable();
            $table->string('email', 100)->unique();
            $table->string('password', 100)->nullable();
            $table->string('role', 25)->default('VISITOR');
            $table->boolean('active')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
