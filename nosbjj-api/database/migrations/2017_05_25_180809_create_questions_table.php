<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->uuid('id')
                ->primary('id');
            $table->string('how_did_you_meet_our_academy',50);
            $table->string('have_you_trained', 50)->nullable();
            $table->string('how_much_time', 50)->nullable();
            $table->string('graduation', 50)->nullable();
            $table->string('team', 50)->nullable();

            $table->string('activity_physical', 20)->nullable();
            $table->string('what_about_health', 20)->nullable();
            $table->boolean('allergic')->default(false);
            $table->string('allergic_descrition', 200)->nullable();
            $table->boolean('medical_certificate')->default(false);
            $table->date('date_certificate')->nullable();
            $table->boolean('authorizes_image_right')->default(false);

            $table->timestamps();
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->string('user_id',36);

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('questions', function ($table) {
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('questions');
    }
}
