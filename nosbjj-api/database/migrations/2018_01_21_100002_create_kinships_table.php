<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKinshipsTable extends Migration
{
    public function up()
    {
        Schema::create('kinships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 35);
        });
    }

    public function down()
    {
        Schema::dropIfExists('kinships');
    }
}
