<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function(){
    Route::group(['prefix' => 'users'], function(){
        Route::get('','UsersController@index');
        Route::post('','UsersController@store');
        Route::get('{id}','UsersController@show');
        Route::put('{id}','UsersController@updateData');
        Route::put('{id}/password','UsersController@updatePassword');
    });

    Route::group(['prefix' => 'maritals-status'], function(){
        Route::get('','MaritalsStatusController@index');
        Route::post('','MaritalsStatusController@store');
        Route::get('{id}','MaritalsStatusController@show');
        Route::put('{id}','MaritalsStatusController@update');
    });

    Route::group(['prefix' => 'kinships'], function(){
        Route::get('','KinshipsController@index');
        Route::post('','KinshipsController@store');
        Route::get('{id}','KinshipsController@show');
        Route::put('{id}','KinshipsController@update');
    });

    Route::group(['prefix' => 'schedule-visits'], function(){
        Route::get('','ScheduleVisitsController@index');
        Route::post('users/{userId}','ScheduleVisitsController@store');
        Route::get('{id}','ScheduleVisitsController@show');
        Route::put('{id}','ScheduleVisitsController@update');
    });

    Route::group(['prefix' => 'questions'], function(){
        Route::get('','QuestionsController@index');
        Route::post('users/{userId}','QuestionsController@store');
        Route::get('{id}','QuestionsController@show');
        Route::put('{id}','QuestionsController@update');
    });

    Route::group(['prefix' => 'registrations'], function(){
        Route::get('','RegistrationsController@index');
        Route::post('','RegistrationsController@store');
        Route::get('{id}','RegistrationsController@show');
        Route::get('{registration_number}','RegistrationsController@show');
        Route::put('{id}','RegistrationsController@update');
        Route::delete('{id}','RegistrationsController@destroy');
    });

});