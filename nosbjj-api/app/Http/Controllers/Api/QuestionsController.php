<?php

namespace App\Http\Controllers\Api;

use App\Models\Questions;
use App\Services\QuestionService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class QuestionsController extends Controller
{
    private $service;

    public function __construct(QuestionService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $questions = $this->service->getPaginateList(15);
        return Response::json($questions, 200)
            ->header('Content-Type','application/json');
    }

    public function show($id)
    {
        $question = $this->service->findById($id);
        return Response::json($question, 200)
            ->header('Content-Type','application/json');
    }

    public function store(Request $request, $userId)
    {
        $question = $this->service->save($request->all(), $userId);
        return Response::json($question, 201)
            ->header('Content-Type','application/json');
    }

    public function update(Request $request, $id)
    {
        $question = $this->service->update($request->all(), $id);
        return Response::json($question, 200)
            ->header('Content-Type','application/json');
    }

}
