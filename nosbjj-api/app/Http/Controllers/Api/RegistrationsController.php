<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\RegistrationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class RegistrationsController extends Controller
{
    private $service;

    public function __construct(RegistrationService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $registrations = $this->service->getPaginateList();
        return Response::json($registrations, 200)
            ->header('Content-Type','application/json');
    }

    public function show($id)
    {
        $registration = $this->service->findById($id);
        return Response::json($registration, 200)
            ->header('Content-Type','application/json');
    }

    public function store(Request $request)
    {
        $data = $this->service->save($request->all());

        return Response::json($data, 201)
            ->header('Content-Type','application/json');
    }

    public function update(Request $request, $id)
    {
        $data = $this->service->save($request->all(), $id);

        return Response::json($data, 200)
            ->header('Content-Type','application/json');
    }

}
