<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Services\UserService;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Response;

class UsersController extends Controller
{
    private $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $users = $this->service->getPaginateList(25);
        return Response::json($users, 200)
            ->header('Content-Type','application/json');
    }

    public function show($id)
    {
        $user = $this->service->findById($id);
        return Response::json($user, 200)
            ->header('Content-Type','application/json');
    }

    public function store(Request $request)
    {
        $data = $this->service->save($request->all());

        return Response::json($data, 201)
            ->header('Content-Type','application/json');
    }

    public function updateData(Request $request, $id)
    {
        $data = $this->service->save($request->all(), $id);

        return Response::json($data, 200)
            ->header('Content-Type','application/json');
    }

    public function updatePassword(Request $request, $id)
    {
//        trecho do código utilizado para testes do update da senha
//        $success = false;
//        $user = Auth::loginUsingId($id);
//
//        if (Auth::check()) {
//            $success = $this->service->updatePassword($request->get('new_password'), $id);
//        }

        $success = $this->service->updatePassword($request->get('new_password'), $id);

        if ($success) {
            return Response::json([
                'msg' => 'Senha atualizada com sucesso.',
            ], 200)
                ->header('Content-Type','application/json');
        } else {
            return Response::json([
                'err' => 'Erro ao tentar atualiar senha',
            ], 400)
                ->header('Content-Type','application/json');
        }
    }

}
