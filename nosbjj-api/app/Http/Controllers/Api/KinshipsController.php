<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Kinship;
use App\Services\KinshipService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class KinshipsController extends Controller
{
    private $service;

    public function __construct(KinshipService $service)
    {
        $this->service = $service;
    }


    public function index()
    {
        $parentescos = $this->service->getPaginateList(15);
        return Response::json($parentescos, 200)
            ->header('Content-Type','application/json');
    }

    public function show($id)
    {
        $parentesco = $this->service->findById($id);
        return Response::json($parentesco, 200)
            ->header('Content-Type','application/json');
    }

    public function store(Request $request)
    {
        $parentesco = $this->service->create($request->all());
        return Response::json($parentesco, 201)
            ->header('Content-Type','application/json');
    }

    public function update(Request $request, $id)
    {
        $parentesco = $this->service->update($request->all(), $id);
        return Response::json($parentesco, 200)
            ->header('Content-Type','application/json');
    }

}
