<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\MaritalStatusService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class MaritalsStatusController extends Controller
{
    private $service;

    public function __construct(MaritalStatusService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $estados = $this->service->getPaginateList(15);
        return Response::json($estados, 200)
            ->header('Content-Type','application/json');
    }

    public function show($id)
    {
        $estado = $this->service->findById($id);
        return Response::json($estado, 200)
            ->header('Content-Type','application/json');
    }

    public function store(Request $request)
    {
        $estado = $this->service->create($request->all());
        return Response::json($estado, 201)
            ->header('Content-Type','application/json');
    }

    public function update(Request $request, $id)
    {
        $estado = $this->service->update($request->all(), $id);
        return Response::json($estado, 200)
            ->header('Content-Type','application/json');
    }

}
