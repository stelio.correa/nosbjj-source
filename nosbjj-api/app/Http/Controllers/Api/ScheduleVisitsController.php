<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\ScheduleVisitService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ScheduleVisitsController extends Controller
{
    private $service;

    public function __construct(ScheduleVisitService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $visits = $this->service->getPaginateList(15);
        return Response::json($visits, 200)
            ->header('Content-Type','application/json');
    }

    public function show($id)
    {
        $visit = $this->service->findById($id);
        return Response::json($visit, 200)
            ->header('Content-Type','application/json');
    }

    public function store(Request $request, $userId)
    {
        $visit = $this->service->save($request->all(), $userId);
        return Response::json($visit, 201)
            ->header('Content-Type','application/json');
    }

    public function update(Request $request, $id)
    {
        $visit = $this->service->update($request->all(), $id);
        return Response::json($visit, 200)
            ->header('Content-Type','application/json');
    }

}
