<?php

namespace App\Models;

use App\Models\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    use Uuids;

    protected $table = 'questions';

    protected $guarded = ['id'];

    public $incrementing = false;

    public $timestamps = true;

    public function getDates()
    {
        return ['date_certificate', 'created_at', 'updated_at'];
    }

    protected $fillable = [
        'how_did_you_meet_our_academy',
        'have_you_trained',
        'how_much_time',
        'graduation',
        'team',

        'activity_physical',
        'what_about_health',
        'allergic',
        'allergic_descrition',
        'medical_certificate',
        'date_certificate',

        'authorizes_image_right',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function setDateCertificateAttribute($date)
    {
        $this->attributes['date_certificate'] = $date == "" ? null :  Carbon::createFromFormat('d/m/Y', $date, 'America/Sao_Paulo');
    }

    public function getDateCertificateAttribute()
    {
        return $this->attributes['date_certificate'] == null ? null : Carbon::parse($this->attributes['date_certificate'])->format('d/m/Y');
    }

}

