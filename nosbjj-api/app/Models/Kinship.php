<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kinship extends Model
{
    protected $table = 'kinships';

    protected $guarded = ['id'];

    public $timestamps = false;

    protected $fillable = [
        'description'
    ];
}
