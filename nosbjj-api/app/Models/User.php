<?php

namespace App\Models;

use App\Models\Traits\Uuids;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable, Uuids;

    protected $table = 'users';

    protected $guarded = ['id'];

    public $incrementing = false;

    protected $fillable = [
        'name',
        'cpf',
        'cellphone',
        'email',
        'password',
        'role',
        'active'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = true;

    public function getDates()
    {
        return ['created_at', 'updated_at'];
    }

    public function scheduleVisits(){
        return $this->hasMany(ScheduleVisits::class, 'user_id','id');
    }

    public function questions(){
        return $this->hasMany(Questions::class, 'user_id','id');
    }

    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = strtolower($email);
    }

    public function setActiveAttribute($value)
    {
        $this->attributes['active'] = $value == true ? 1 : 0;
    }

    public function getActiveAttribute()
    {
        return $this->attributes['active'] == 1 ? true : false;
    }

    public function getFirstName(){
        $firstName = substr($this->name, 0, 1) . substr(strtolower(explode(' ', $this->name)[0]), 1, strlen($this->name) - 1);;
        return $firstName;
    }

    public function activate()
    {
        $this->active = true;
        $this->save();
    }

}
