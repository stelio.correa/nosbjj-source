<?php

namespace App\Models;

use App\Models\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    use Uuids;

    protected $table = 'registrations';

    protected $guarded = ['id'];

    public $incrementing = false;

    protected $fillable = [
        'registration_number',
        'name',
        'gender',
        'date_of_birth',
        'cpf',
        'profession',
        'degree_education',
        'email',
        'rg_number',
        'rg_emitter',
        'phone',
        'cellphone',
        'cep',
        'address',
        'number',
        'address_complement',
        'neighborhood',
        'city',
        'country',

        'user_id',
        'marital_status_id',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = true;

    public function getDates()
    {
        return ['date_of_birth', 'created_at', 'updated_at'];
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function maritalStatus(){
        return $this->belongsTo(MaritalStatus::class, 'marital_status_id');
    }

    public function setDateOfBirthAttribute($date)
    {
        $this->attributes['date_of_birth'] = $date == "" ? null :  Carbon::createFromFormat('d/m/Y', $date, 'America/Sao_Paulo');
    }

    public function getDateOfBirthAttribute()
    {
        return $this->attributes['date_of_birth'] == null ? null : Carbon::parse($this->attributes['date_of_birth'])->format('d/m/Y');
    }

    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = strtolower($email);
    }

    public function setActiveAttribute($value)
    {
        $this->attributes['active'] = $value == true ? 1 : 0;
    }

    public function getActiveAttribute()
    {
        return $this->attributes['active'] == 1 ? true : false;
    }

}
