<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaritalStatus extends Model
{
    protected $table = 'maritals_status';

    protected $guarded = ['id'];

    public $timestamps = false;

    protected $fillable = [
        'description'
    ];

}
