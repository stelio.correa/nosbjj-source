<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Traits\Uuids;
use Carbon\Carbon;

class ScheduleVisits extends Model
{
    use Uuids;

    protected $table = 'schedule_visits';

    protected $guarded = ['id'];

    public $incrementing = false;

    protected $fillable = [
        'visit_date',
        'visit_time',
        'user_id'
    ];

    protected $dates = ['visit_date','visit_time','created_at','updated_at'];

    public $timestamps = true;

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function setVisitDateAttribute($date)
    {
        $this->attributes['visit_date'] = $date == "" ? null :  Carbon::createFromFormat('d/m/Y', $date, 'America/Sao_Paulo');
    }

    public function getVisitDateAttribute()
    {
        return $this->attributes['visit_date'] == null ? null : Carbon::parse($this->attributes['visit_date'])->format('d/m/Y');
    }

    public function setVisitTimeAttribute($date)
    {
        $this->attributes['visit_time'] = $date == "" ? null :  Carbon::createFromFormat('H:m', $date, 'America/Sao_Paulo');
    }

    public function getVisitTimeAttribute()
    {
        return $this->attributes['visit_time'] == null ? null : Carbon::parse($this->attributes['visit_time'])->format('H:i');
    }

    public function markPresence()
    {
        $this->status = 'P';
        $this->save();
    }

    public function undoPresence()
    {
        $this->status = 'F';
        $this->save();
    }

}
