<?php

namespace App\Services;


use App\Models\User;
use App\Services\Traits\Service;

class UserService
{
    use Service;

    private $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function save($data, $id = null)
    {
        $data = $this->preparesData($data);

        if ($id)
            return $this->update($data, $id);

        return $this->create($data);
    }

    public function updatePassword($newPassword, $id)
    {
        $user = array_merge($this->model->find($id)->toArray(), ['password' => bcrypt($newPassword)]);

        return $this->update($user, $id);
    }

    private function preparesData($data)
    {
        if (isset($data['cellphone']))
            $data['cellphone'] = $this->cleanCharacteres($data['cellphone']);

        if (isset($data['cpf']))
            $data['cpf'] = $this->cleanCharacteres($data['cpf']);

        return $data;
    }

    public function cadastralDouble($cpf, $email)
    {
        $userCpfNotEmail   = $this->model->where('cpf', [$cpf])
            ->orWhereIn('email', [$email])
            ->get(['id','cpf', 'email'])->count();

        $userCpfAndEmail = $this->model->where('cpf', [$cpf])
            ->where('email', $email)
            ->get(['id','cpf', 'email'])->count();

        return $userCpfNotEmail != 0 && $userCpfAndEmail != 1;
    }

    private function cleanCharacteres($term)
    {
        $pattern = "/\D/";

        return preg_replace($pattern,"", $term);
    }

    public function avaliar()
    {
        if($user == null){
            $user = User::create($request->all());
        } else {
            if($user->role == 'ADMINISTRADOR'){
                return redirect()
                    ->action('Visits\VisitsController@create')
                    ->with('warning', "Oops!!! ".$user->getFirstName().", tu és ADMIN, manooo!!!")
                    ->withInput();
            }

            $scheduleVisit = ScheduleVisits::where('user_id', $user->id)->get()->last();

            if($scheduleVisit->status == 'A'){
                return redirect()
                    ->action('Visits\VisitsController@create')
                    ->with('warning', "Oops!!! ".$user->getFirstName().", você tem uma visita aberta para o dia ".$scheduleVisit->visit_date.". Poderá agenda nova após a visita.")
                    ->withInput();
            }

            $bloqueado = $this->bloquearVisita($user->id,$request->get('visit_date'));

            if($bloqueado['bloqueado']){
                return redirect()
                    ->action('Visits\VisitsController@create')
                    ->with('warning', "Ops!!! Desculpe-nos, ".explode(' ', $request->get('name'))[0].", mas não pode agendar nova visita. Poderá agendar a partir do dia ".$bloqueado['next_visit'])
                    ->withInput();
            }
        }
    }
}