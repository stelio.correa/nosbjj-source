<?php

namespace App\Services;


use App\Models\Questions;
use App\Services\Traits\Service;

class QuestionService
{
    use Service;

    private $model;
    private $userSrv;

    public function __construct(Questions $model, UserService $userSrv)
    {
        $this->model = $model;
        $this->userSrv = $userSrv;
    }

    public function getPaginateList($perPage = 25, array $colmns = null)
    {
        if ($colmns)
            return $this->model->with('user')->paginate($perPage, $colmns);

        return $this->model->with('user')->paginate($perPage);
    }

    public function findById($id)
    {
        return $this->model->with('user')->find($id);
    }

    public function save(array $data, $userId)
    {
        $user = $this->userSrv->findById($userId);
        $question = $this->create(array_merge($data,['user_id'=>$user->id]));

        return $question;
    }

}