<?php

namespace App\Services;


use App\Models\ScheduleVisits;
use App\Services\Traits\Service;
use Carbon\Carbon;

class ScheduleVisitService
{
    use Service;

    private $model;
    private $userSrv;

    public function __construct(ScheduleVisits $model, UserService $userSrv)
    {
        $this->model = $model;
        $this->userSrv = $userSrv;
    }

    public function getPaginateList($perPage = 25, array $colmns = null)
    {
        if ($colmns)
            return $this->model->with('user')->paginate($perPage, $colmns);

        return $this->model->with('user')->paginate($perPage);
    }

    public function findById($id)
    {
        return $this->model->with('user')->find($id);
    }

    public function fullClass($date, $time)
    {
        $dataArray = explode('/',$date);
        $dia = date("Y-m-d", mktime(0, 0, 0, $dataArray[1], $dataArray[0], $dataArray[2]));
        $schedule = $this->model->where('visit_date', $dia)
            ->where('visit_time', $time)
            ->get()->count();

        return $schedule >= 3;
    }

    public function blockedVisitor($userId, $data)
    {
        $scheduleVisit = $this->model
            ->where('user_id', $userId)
            ->orderby('id', 'desc')->first();
        $lastVisit = Carbon::createFromFormat('d/m/Y', $scheduleVisit->visit_date, 'America/Sao_Paulo');
        $newVisit  = Carbon::createFromFormat('d/m/Y', $data, 'America/Sao_Paulo');
        $nextVisit = $lastVisit->addDays(10);

        return [
            'block' => $newVisit->ne($lastVisit) && !$newVisit->gt($lastVisit),
            'next_visit'=> Carbon::parse($nextVisit)->format('d/m')
        ];
    }

    public function dateLessThanCurrent($date)
    {
        $currentDate = Carbon::now('America/Sao_Paulo');
        $visitDate = Carbon::createFromFormat('d/m/Y', $date, 'America/Sao_Paulo');

        return $visitDate->lt($currentDate);
    }

    public function save(array $data, $userId)
    {
        $user = $this->userSrv->findById($userId);
        $dataPassada = $this->dateLessThanCurrent($data['visit_date']);

        if($dataPassada){
            return [
                'wrn' => "Data anterior a atual",
                'msg' => "Ops!!! Desculpe-nos, ".$user->getFirstName().", a data informada já passou. Tente outra data."
            ];
        }

        $scheduleVisit = $this->model->where('user_id', $userId)->get()->last();

        if ($scheduleVisit) {
            if($scheduleVisit->status == 'A'){
                return [
                    'wrn' => "Visita em aberto",
                    'msg' => "Oops!!! ".$user->getFirstName().", você tem uma visita aberta para o dia ".$scheduleVisit->visit_date.". Poderá agenda nova após a visita."
                ];
            }

            $bloqueado = $this->blockedVisitor($userId, $data['visit_date']);

            if($bloqueado['bloqueado']){
                return [
                    'wrn' => "Dentro do intervalo entre visitas",
                    'msg' => "Ops!!! Desculpe-nos, ".$user->getFirstName().", mas não pode agendar nova visita. Poderá agendar a partir do dia ".$bloqueado['next_visit']
                ];
            }

            $turmaCheia = $this->fullClass($data['visit_date'],$data['visit_time']);
            if($turmaCheia){
                return [
                    'wrn' => "Limite de visitante completo",
                    'msg' => "Ops!!! Desculpe-nos, ".$user->getFirstName().", nosso limite de visitas para esse horário foi atingido."
                ];
            }
        }

        $scheduleVisit = $this->create(array_merge($data,['user_id' => $userId]));

        return $scheduleVisit;
    }

}