<?php

namespace App\Services;


use App\Models\Kinship;
use App\Services\Traits\Service;

class KinshipService
{
    use Service;

    private $model;

    public function __construct(Kinship $model)
    {
        $this->model = $model;
    }

}