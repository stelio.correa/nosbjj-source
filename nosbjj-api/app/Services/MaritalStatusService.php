<?php

namespace App\Services;


use App\Models\MaritalStatus;
use App\Services\Traits\Service;

class MaritalStatusService
{
    use Service;

    private $model;

    public function __construct(MaritalStatus $model)
    {
        $this->model = $model;
    }

}