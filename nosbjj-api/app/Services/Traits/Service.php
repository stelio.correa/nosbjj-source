<?php

namespace App\Services\Traits;


trait Service
{
    public function getList(array $colmns = null)
    {
        if ($colmns)
            return $this->model->all($colmns);

        return $this->model->all();
    }

    public function getPaginateList($perPage = 25, array $colmns = null)
    {
        if ($colmns)
            return $this->model->paginate($perPage, $colmns);

        return $this->model->paginate($perPage);
    }

    public function findById($id)
    {
        return $this->model->find($id);
    }

    public function findByField($field, $term, $like = false)
    {
        if ($like)
            return $this->model->where($field, 'like', '%'.$term.'%')->get();

        return $this->model->where($field, $term)->get();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        $recove = $this->model->find($id);
        $recove->update(array_merge($recove->toArray(), $data));

        return $recove;
    }

}
