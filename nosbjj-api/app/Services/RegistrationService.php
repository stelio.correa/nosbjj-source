<?php

namespace App\Services;


use App\Models\Questions;
use App\Models\Registration;
use App\Services\Traits\Service;

class RegistrationService
{
    use Service;

    private $model;
    private $userSrv;

    public function __construct(Registration $model, UserService $userSrv)
    {
        $this->model = $model;
        $this->userSrv = $userSrv;
    }

    public function getPaginateList($perPage = 25, array $colmns = null)
    {
        if ($colmns)
            return $this->model->with(['user', 'maritalStatus'])->paginate($perPage, $colmns);

        return $this->model->with(['user', 'maritalStatus'])->paginate($perPage);
    }

    public function findById($id)
    {
        return $this->model->with('user')->find($id);
    }

    public function save($data, $id = null)
    {
        $data = $this->preparesData($data);

        if ($id)
            return $this->update($data, $id);

        return $this->create($data);
    }

    private function preparesData($data)
    {
        if (isset($data['cellphone']))
            $data['cellphone'] = $this->cleanCharacteres($data['cellphone']);

        if (isset($data['cpf']))
            $data['cpf'] = $this->cleanCharacteres($data['cpf']);

        if (isset($data['phone']))
            $data['phone'] = $this->cleanCharacteres($data['phone']);

        if (isset($data['cep']))
            $data['cep'] = $this->cleanCharacteres($data['cep']);

        return $data;
    }

    private function cleanCharacteres($term)
    {
        $pattern = "/\D/";

        return preg_replace($pattern,"", $term);
    }

}