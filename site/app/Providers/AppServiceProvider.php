<?php

namespace App\Providers;

use App\Mail\Domains\Visits\Confirmation;
use Illuminate\Support\ServiceProvider;

use App\Models\ScheduleVisits;
use App\Mail\Domains\Visits\NoShow;
use App\Mail\Domains\Visits\ShowedUp;

use Mail;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ScheduleVisits::created(function($visit){
            Mail::to($visit->user)->send(new Confirmation($visit->user));
        });

        ScheduleVisits::updated(function($visit){
            if($visit->status == 'P'){
                Mail::to($visit->user)->send(new ShowedUp($visit->user));
            } elseif ($visit->status == 'F'){
                Mail::to($visit->user)->send(new NoShow($visit->user));
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
