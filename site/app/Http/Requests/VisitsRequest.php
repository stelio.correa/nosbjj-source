<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VisitsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            //'date_of_birth' => 'required|date|date_format:d/m/Y',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'cpf' => 'required',
            'cellphone' => 'required',
            'email' => 'required',
            'how_did_you_meet' => 'required',
            'visit_date' => 'required',
            'visit_time' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nome é obrigatório.',
            'date_of_birth.required' => 'Data de nascimento é obrigatório.',
            /*'date_of_birth.date' => 'Data inválida.',
            'date_of_birth.date_format' => 'Formato inválido(dd/mm/aaaa).',*/
            'gender.required' => 'Sexo é obrigatório.',
            'cpf.required' => 'CPF é obrigatório.',
            'cellphone.required' => 'Celular é obrigatório.',
            'email.required' => 'E-mail é obrigatório.',
            'how_did_you_meet.required' => 'Como conheceu a Nós BJJ é obrigatório.',
            'visit_date.required' => 'Data da visita é obrigatório.',
            'visit_time.required' => 'Hora da visita é obrigatório.'
        ];
    }
}
