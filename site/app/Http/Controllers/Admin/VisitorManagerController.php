<?php

namespace App\Http\Controllers\Admin;

use App\Models\ScheduleVisits;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VisitorManagerController extends Controller
{
    public function index()
    {
        $visits = ScheduleVisits::orderby('id','asc')->paginate(10);

        return view('admin.visits.list')->with('visits', $visits);
    }

    public function markPresence($id)
    {
        $visit = ScheduleVisits::find($id);
        $visit->markPresence();

        return redirect()
            ->action('Admin\VisitorManagerController@index')
            ->with('success', 'Registro realizado com sucesso!!!');
    }

    public function undoPresence($id)
    {
        $visit = ScheduleVisits::find($id);
        $visit->undoPresence();

        return redirect()
            ->action('Admin\VisitorManagerController@index')
            ->with('success', 'Registro realizado com sucesso!!!');
    }

}
