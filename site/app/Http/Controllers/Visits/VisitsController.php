<?php

namespace App\Http\Controllers\Visits;

use App\Http\Controllers\Controller;
use App\Http\Requests\VisitsRequest;
use App\Models\Questions;
use App\Models\ScheduleVisits;
use App\Models\User;
use Carbon\Carbon;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class VisitsController extends Controller
{

    public function create()
    {
        return view('visits.new');
    }

    private function turmaCheia($date, $time){

        $dataArray = explode('/',$date);

        $dia = date("Y-m-d", mktime(0, 0, 0, $dataArray[1], $dataArray[0], $dataArray[2]));

        $schedule =  ScheduleVisits::where('visit_date', $dia)
            ->where('visit_time', $time)
            ->get()->count();

        return $schedule >= 3;
    }

    private function bloquearVisita($userId, $data){
        $scheduleVisit = ScheduleVisits::where('user_id', $userId)->orderby('id', 'desc')->first();

        $lastVisit = Carbon::createFromFormat('d/m/Y', $scheduleVisit->visit_date, 'America/Sao_Paulo');
        $newVisit  = Carbon::createFromFormat('d/m/Y', $data, 'America/Sao_Paulo');

        $nextVisit = $lastVisit->addDays(10);

        return [
            'bloqueado' => $newVisit->ne($lastVisit) && !$newVisit->gt($lastVisit),
            'next_visit'=> Carbon::parse($nextVisit)->format('d/m')
        ];
    }

    private function dataMenorQueCorrente($data){
        $currentDate = Carbon::now('America/Sao_Paulo');
        $visitDate = Carbon::createFromFormat('d/m/Y', $data, 'America/Sao_Paulo');

        return $visitDate->lt($currentDate);
    }

    private function duplicidadeCadastral($cpf, $email)
    {
        $userCpfNotEmail   = User::where('cpf', [$cpf])
            ->orWhereIn('email', [$email])
            ->get(['id','cpf', 'email'])->count();

        $userCpfAndEmail = User::where('cpf', [$cpf])
            ->where('email', $email)
            ->get(['id','cpf', 'email'])->count();

        return $userCpfNotEmail != 0 && $userCpfAndEmail != 1;
    }

    public function store(VisitsRequest $request)
    {
        $dataPassada = $this->dataMenorQueCorrente($request->get('visit_date'));
        $duplicidadeCadastral = $this->duplicidadeCadastral($request->get('cpf'), $request->get('email'));

        if($dataPassada){

            return redirect()
                ->action('Visits\VisitsController@create')
                ->with('error', "Ops!!! Desculpe-nos, ".explode(' ', $request->get('name'))[0].", a data informada já passou. Tente outra data.")
                ->withInput();
        }

        if($duplicidadeCadastral){
            return redirect()
                ->action('Visits\VisitsController@create')
                ->with('error', "Ops!!! ".explode(' ', $request->get('name'))[0].", você possui cadastro conosco utilizando seu CPF ou E-mail. Tente novamente com outro e-mail, caso persista o erro entre em contato conosco. Obrigado.")
                ->withInput();
        }

        $user   = User::where('cpf', $request->get('cpf'))->get(['id','cpf', 'email'])->first();

        if($user == null){
            $user = User::create($request->all());
        } else {
            if($user->role == 'ADMINISTRADOR'){
                return redirect()
                    ->action('Visits\VisitsController@create')
                    ->with('warning', "Oops!!! ".$user->getFirstName().", tu és ADMIN, manooo!!!")
                    ->withInput();
            }

            $scheduleVisit = ScheduleVisits::where('user_id', $user->id)->get()->last();

            if($scheduleVisit->status == 'A'){
                return redirect()
                    ->action('Visits\VisitsController@create')
                    ->with('warning', "Oops!!! ".$user->getFirstName().", você tem uma visita aberta para o dia ".$scheduleVisit->visit_date.". Poderá agenda nova após a visita.")
                    ->withInput();
            }

            $bloqueado = $this->bloquearVisita($user->id,$request->get('visit_date'));

            if($bloqueado['bloqueado']){
                return redirect()
                    ->action('Visits\VisitsController@create')
                    ->with('warning', "Ops!!! Desculpe-nos, ".explode(' ', $request->get('name'))[0].", mas não pode agendar nova visita. Poderá agendar a partir do dia ".$bloqueado['next_visit'])
                    ->withInput();
            }
        }

        $turmaCheia = $this->turmaCheia($request->get('visit_date'),$request->get('visit_time'));
        if($turmaCheia){
            return redirect()
                ->action('Visits\VisitsController@create')
                ->with('error', "Ops!!! Desculpe-nos, ".explode(' ', $request->get('name'))[0].", nosso limite de visitas para esse horário foi atingido.")
                ->withInput();
        }

        $question = Questions::create(array_merge($request->all(),['user_id'=>$user->id]));
        $scheduleVisit = ScheduleVisits::create(array_merge($request->all(),['user_id'=>$user->id]));

        return redirect()
            ->action('Visits\VisitsController@create')
            ->with('success', 'Registro realizado com sucesso!!!');
    }

    public function baixarFormMatricula()
    {
        $path = storage_path('app/arquivos/formularios/form_matricula.pdf');
        $headers = [
            "content-type: application/pdf"
        ];
        $fileName = 'Formulário para matrícula.pdf';

        return Response::download($path, $fileName, $headers);
    }

}
