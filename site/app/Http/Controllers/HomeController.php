<?php

namespace App\Http\Controllers;

use App\Models\ScheduleVisits;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $start;
    private $end;

    public function setWeekPeriod($weeknumber)
    {
        $week_start = (new DateTime())->setISODate(date("Y"),$weeknumber)->format("Y-m-d H:i:s");

        $this->start = Carbon::createFromFormat("Y-m-d H:i:s", $week_start);
        $this->start->hour(0)->minute(0)->second(0);
        $this->end = $this->start->copy()->endOfWeek();
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $visits = $this->scheduleWeek();

        return view('home')
            ->with('visits', $visits);
    }

    private function scheduleWeek()
    {
        $currentDay = Carbon::now('America/Sao_Paulo');
        $startWeek = $currentDay->startOfWeek();

        $visits = [
            'seg' => [
                'dia_semana' => 'Segunda-feira',
                'dia' => $startWeek->format('d/m'),
                'visits' => $this->countVisitsDate($startWeek)
            ],
            'ter' => [
                'dia_semana' => 'Terça-feira',
                'dia' => $startWeek->addDays(1)->format('d/m'),
                'visits' => $this->countVisitsDate($startWeek)
            ],
            'qua' => [
                'dia_semana' => 'Quarta-feira',
                'dia' => $startWeek->addDays(1)->format('d/m'),
                'visits' => $this->countVisitsDate($startWeek)
            ],
            'qui' => [
                'dia_semana' => 'Quinta-feira',
                'dia' => $startWeek->addDays(1)->format('d/m'),
                'visits' => $this->countVisitsDate($startWeek)
            ],
            'sex' => [
                'dia_semana' => 'Sexta-feira',
                'dia' => $startWeek->addDays(1)->format('d/m'),
                'visits' => $this->countVisitsDate($startWeek)
            ],
        ];

        return $visits;

    }

    private function countVisitsDate($date)
    {
        return ScheduleVisits::where('visit_date',$date)->count();
    }

}
