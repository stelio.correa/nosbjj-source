<?php

namespace App\Models;

use App\Notifications\ResetPasswordNotification;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    protected $guarded = ['id'];

    protected $fillable = [
        'name',
        'date_of_birth',
        'gender',
        'cpf',
        'phone',
        'cellphone',
        'email',
        'password',
        'role',
        'active'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = true;

    public function getDates()
    {
        return ['date_of_birth', 'created_at', 'updated_at'];
    }

    public function scheduleVisits(){
        return $this->hasMany(ScheduleVisits::class, 'user_id','id');
    }

    public function questions(){
        return $this->hasMany(Questions::class, 'user_id','id');
    }

    public function setDateOfBirthAttribute($date)
    {
        $this->attributes['date_of_birth'] = $date == "" ? null :  Carbon::createFromFormat('d/m/Y', $date, 'America/Sao_Paulo');
    }

    public function getDateOfBirthAttribute()
    {
        return $this->attributes['date_of_birth'] == null ? null : Carbon::parse($this->attributes['date_of_birth'])->format('d/m/Y');
    }

    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = strtolower($email);
    }

    public function getFirstName(){
        $firstName = substr($this->name, 0, 1) . substr(strtolower(explode(' ', $this->name)[0]), 1, strlen($this->name) - 1);;
        return $firstName;
    }

    public function activate()
    {
        $this->active = true;
        $this->save();
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

}
