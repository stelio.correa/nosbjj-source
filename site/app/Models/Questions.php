<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{

    protected $table = 'questions';

    protected $guarded = ['id'];

    protected $fillable = [
        'how_did_you_meet',
        'have_you_trained',
        'team',
        'graduation',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
