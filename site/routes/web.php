<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'Visits'], function(){

    Route::get('agendamento', 'VisitsController@create')->name('visits.new');

    Route::post('agendamento/store', 'VisitsController@store')
        ->name('visits.store');

    Route::get('/formularios/matricula','VisitsController@baixarFormMatricula')
        ->name('matricula.download');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'painel'], function(){
    Route::get('visitas', 'VisitorManagerController@index')->name('admin.visits');
    Route::get('/marcar-presenca/{id}', 'VisitorManagerController@markPresence')->where('id', '[0-9]+')->name('admin.mark.presence');
    Route::get('/desmarcar-presenca/{id}', 'VisitorManagerController@undoPresence')->where('id', '[0-9]+')->name('admin.mark.lack');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
