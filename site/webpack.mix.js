const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/assets/js/app.js', 'public/js')
    .copy('node_modules/pickadate/lib/picker.js', 'public/js')
    .copy('node_modules/pickadate/lib/picker.date.js', 'public/js')
    .copy('node_modules/pickadate/lib/picker.time.js', 'public/js')
    .copy('resources/assets/js/pickadate.js', 'public/js')
    .copy('resources/assets/img', 'public/img')
    .copy('node_modules/font-awesome/fonts', 'public/fonts')
    // .copy('node_modules/jquery-mask-plugin/dist/jquery.mask.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');
