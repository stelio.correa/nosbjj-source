<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Mail\Domains\Visits\Confirmation;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Mail;

class EnviaEmailConfirmacaoTest extends TestCase
{
    public function testExample()
    {
        $user = User::where('email', 'stelio.correa@gmail.com')->get()->first();

        Mail::to($user->email)->send(new Confirmation($user));

        $this->assertTrue(true);
    }
}
