<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Mail\Domains\Visits\NoShow;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use Mail;

class EnviaEmailNoShowTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = User::where('email', 'stelio.correa@gmail.com')->get()->first();

        Mail::to($user->email)->send(new NoShow($user));

        $this->assertTrue(true);
    }
}
