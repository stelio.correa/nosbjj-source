$('.datepicker').pickadate({

    // Formats
    format: 'dd/mm/yyyy',
    formatSubmit: undefined,
    hiddenPrefix: undefined,
    hiddenSuffix: '_submit',
    hiddenName: undefined,

    selectYears: 130,
    selectMonths: true,

    // Close on a user action
    closeOnSelect: true,
    closeOnClear: true,

    today: false,

    // Strings and translations
    labelMonthNext: 'Próximo mês',
    labelMonthPrev: 'Mês anterior',
    monthsFull: ['Janiero', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
    showMonthsShort: undefined,
    showWeekdaysFull: undefined,

    // Buttons
    today: '',
    clear: 'Limpar',
    close: 'Fechar',

    //disable: [1]

});

$('.timepicker').pickatime({
    // Translations and clear button
    clear: 'Limpar',

    // Formats
    format: 'h:i A',

    min: [7,0],
    max: [21,0],

    interval: 100,

    disable: [
        8, 11, 13, 14, 16, 18, 20
    ],

    onOpen: function() {
        console.log('Limpando horas desabilitadas...');
        $('.picker__list-item--disabled').hide();
    },

    onSet: function(context) {
        console.log('Just set stuff:', context)
    }

});