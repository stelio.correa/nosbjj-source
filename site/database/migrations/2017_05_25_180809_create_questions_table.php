<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('how_did_you_meet',50);
            $table->string('have_you_trained', 50)->nullable();
            $table->string('team', 50)->nullable();
            $table->string('graduation', 50)->nullable();
            $table->timestamps();
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('questions', function ($table) {
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('questions');
    }
}
