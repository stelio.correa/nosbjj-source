<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleVisitsTable extends Migration
{
    public function up()
    {
        Schema::create('schedule_visits', function (Blueprint $table) {
            $table->increments('id');
            $table->date('visit_date');
            $table->time('visit_time');
            $table->char('status', 1)->default('A');
            $table->timestamps();
        });

        Schema::table('schedule_visits', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('schedule_visits', function ($table) {
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('schedule_visits');
    }
}
