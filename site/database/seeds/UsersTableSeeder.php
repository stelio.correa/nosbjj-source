<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'name' => 'ADMINISTRADOR DO SISTEMA',
                'gender' => 'M',
                'cpf' => '000.000.000-00',
                'email' => 'contato@nosbjj.com.br',
                'password' => bcrypt('#Adm1n'),
                'active' => true,
                'role' => 'ADMINISTRADOR'
            ]
        ];
//        DB::table('users')->truncate();
        DB::table('users')->insert($data);
    }
}
