require('./bootstrap');

$(document).ready(function(){
    // $("input[type='date']").mask('00/00/0000');
    $("input[type='time']").mask('00:00');
    $(".phone").mask('(99) 9999-9999');
    $(".cellphone").mask('(99) 9 9999-9999');
    $(".cep").mask('99.999-999');
    $(".cpf").mask('999.999.999-99');
});