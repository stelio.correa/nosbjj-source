@if ($message = Session::get('success'))

    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-success alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                <p>{{ $message }}</p>

            </div>
        </div>
    </div>

@endif


@if ($message = Session::get('error'))

    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                <p>{{ $message }}</p>

            </div>
        </div>
    </div>

@endif


@if ($message = Session::get('warning'))

    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-warning alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                <p>{{ $message }}</p>

            </div>
        </div>
    </div>

@endif


@if ($message = Session::get('info'))

    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                <p>{{ $message }}</p>

            </div>
        </div>
    </div>

@endif


@if ($errors->any())

    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger">

                <button type="button" class="close" data-dismiss="alert">×</button>

                Por favor, verifique os campos em destaque do formulário abaixo para erros.

                {{--<pre>{{ var_dump($errors) }}</pre>--}}

            </div>
        </div>
    </div>

@endif