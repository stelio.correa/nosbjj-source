@extends('layouts.default')

@section('content')

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-content" style="padding: 25px;">

                    <div class="row">
                        <div class="col-md-5">
                            <h3 style="font-size: 4em;">Oops!!!</h3>
                        </div>
                        <div class="col-md-7">
                            <div class="text-center">
                        <span style="font-size: 2em;">
                        A página que você está procurando não foi encontrada.
                    </span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <div class="text-center">
                                <a href="{{ url('/') }}" class="btn btn-default btn-lg">Voltar</a>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>

@endsection