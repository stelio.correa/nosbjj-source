@extends('layouts.admin')

@section('title', 'Listagem de visitas')

@section('content')
    <header>
        <div class="page-header">
            <h1>Listagem de Visitas</h1>
        </div>
    </header>

    <section>
        @include('includes.alerts')
    </section>

    <section>
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="text-center">CPF</th>
                <th>NOME</th>
                <th>E-MAIL</th>
                <th class="text-center">DATA</th>
                <th class="text-center">HORA</th>
                <th class="text-center">SITUAÇÃO</th>
                <th class="text-center" colspan="2">AÇÕES</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($visits as $visit)
                <tr>
                    <td class="text-center">{{ $visit->user->cpf }}</td>
                    <td>{{ $visit->user->name }}</td>
                    <td>{{ $visit->user->email }}</td>
                    <td class="text-center">{{ $visit->visit_date }}</td>
                    <td class="text-center">{{ $visit->visit_time }}</td>
                    <td class="text-center">
                        @if($visit->status == 'P')
                            <span class="label label-success">
                                Presença
                            </span>
                        @elseif($visit->status == 'F')
                            <span class="label label-danger">
                                Faltou
                            </span>
                        @else
                            <span class="label label-default">
                                Aberto
                            </span>
                        @endif
                    </td>
                    <td class="text-center">
                        <a href="{{ action('Admin\VisitorManagerController@markPresence', $visit->id) }}"
                           title="Marcar Presença" class="text-success">
                            <i class="fa fa-check-square-o fa-2x"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="{{ action('Admin\VisitorManagerController@undoPresence', $visit->id) }}"
                           title="Marcar Falta" class="text-danger">
                            <i class="fa fa-remove fa-2x"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="row">
            <div class="col-md-12 text-center">
                {{ $visits->links() }}<br/>
                <div style="margin-top: -20px;">
                    <small>{{ 'Página: '.$visits->currentPage().' de '.$visits->lastPage()}}</small>
                </div>
            </div>
        </div>
    </section>
@endsection