@extends('layouts.admin')

@section('title', 'Painel Administrativo')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="page-header">
                <h2>Agendamentos da Semana</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        @foreach($visits as $visit)
        <div class="col-md-2">
            <div class="panel panel-info">
                <div class="panel-heading">{{ $visit['dia_semana'] }}</div>
                <div class="panel-body text-center">
                    <h1>{{ $visit['dia'] }}</h1>
                </div>
                <div class="panel-footer">
                    Total de visitas: {{ $visit['visits'] }}
                </div>
            </div>
        </div>
        @endforeach
        <div class="col-md-1"></div>
    </div>
@endsection
