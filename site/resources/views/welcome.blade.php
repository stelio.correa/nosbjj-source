@extends('layouts.default')

@section('title', 'Seja Bem Vindo!')

@section('header-content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                <img src="{{ asset('/img/logo.png') }}" alt="NÓS BJJ" class="img-responsive">
            </div>

            <div class="row">
                <div class="col-md-6">
                    <a href="{{ route('visits.new') }}" class="btn btn-warning nosbjj-btn-orange btn-lg btn-block">
                        <i class="fa fa-calendar"></i> VISITA
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('matricula.download') }}" class="btn btn-warning nosbjj-btn-black btn-lg btn-block">
                        <i class="fa fa-download"></i> MATRÍCULA
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

@endsection