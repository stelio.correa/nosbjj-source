@extends('layouts.default')

@section('title', 'Agendar visita')

@section('content')

    <h2>Agendar Visita</h2>

    <section>
        @include('includes.alerts')
    </section>

    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('visits.store') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                                <div class="form-group">

                                    <div class="col-md-6{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="control-label">Nome completo</label>
                                        <input type="text" id="name" name="name" value="{{ old('name') }}"
                                               class="form-control" autofocus>
                                        @if ($errors->has('name'))
                                            <span class="help-block">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-3{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                                        <label for="date_of_birth" class="control-label">Nascimento</label>
                                        <input type="date" id="date_of_birth" name="date_of_birth"  value="{{ old('date_of_birth') }}"
                                               class="form-control datepicker">
                                        @if ($errors->has('date_of_birth'))
                                            <span class="help-block">{{ $errors->first('date_of_birth') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-3{{ $errors->has('gender') ? ' has-error' : '' }}">
                                        <label for="gender" class="control-label">Sexo</label>
                                        <select id="gender" name="gender" class="form-control">
                                            <option value="" selected disabled>Selecione</option>
                                            <option value="M" {{ old('gender') == 'M' ? 'selected':'' }}>Masculino</option>
                                            <option value="F" {{ old('gender') == 'F' ? 'selected':'' }}>Feminino</option>
                                        </select>
                                        @if ($errors->has('gender'))
                                            <span class="help-block">{{ $errors->first('gender') }}</span>
                                        @endif
                                    </div>

                                </div>

                                <div class="form-group">

                                    <div class="col-md-3{{ $errors->has('cpf') ? ' has-error' : '' }}">
                                        <label for="cpf" class="control-label">CPF</label>
                                        <input type="text" id="cpf" name="cpf" value="{{ old('cpf') }}"
                                               class="form-control cpf">
                                        @if ($errors->has('cpf'))
                                            <span class="help-block">{{ $errors->first('cpf') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-3{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label for="phone" class="control-label">Telefone</label>
                                        <input type="text" id="phone" name="phone" value="{{ old('phone') }}"
                                               class="form-control phone">
                                        @if ($errors->has('phone'))
                                            <span class="help-block">{{ $errors->first('phone') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-3{{ $errors->has('cellphone') ? ' has-error' : '' }}">
                                        <label for="cellphone" class="control-label">Celular</label>
                                        <input type="text" id="cellphone" name="cellphone" value="{{ old('cellphone') }}"
                                               class="form-control cellphone">
                                        @if ($errors->has('cellphone'))
                                            <span class="help-block">{{ $errors->first('cellphone') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-3{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="control-label">E-mail</label>
                                        <input type="text" id="email" name="email" value="{{ old('email') }}"
                                               class="form-control">
                                        @if ($errors->has('email'))
                                            <span class="help-block">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>

                                </div>

                                <div class="form-group">

                                    <div class="col-md-6{{ $errors->has('how_did_you_meet') ? ' has-error' : '' }}">
                                        <label for="how_did_you_meet" class="control-label">Como conheceu a Nós BJJ?</label>
                                        <select id="how_did_you_meet" name="how_did_you_meet" class="form-control">
                                            <option value="" selected disabled>Selecione</option>
                                            <option value="A"{{ old('how_did_you_meet') == 'A' ? ' selected':'' }}>Amigo(a)</option>
                                            <option value="C"{{ old('how_did_you_meet') == 'C' ? ' selected':'' }}>Competições</option>
                                            <option value="R"{{ old('how_did_you_meet') == 'R' ? ' selected':'' }}>Redes Sociais</option>
                                        </select>
                                        @if ($errors->has('how_did_you_meet'))
                                            <span class="help-block">{{ $errors->first('how_did_you_meet') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-6{{ $errors->has('have_you_trained') ? ' has-error' : '' }}">
                                        <label for="have_you_trained" class="control-label">Já treinou?</label>
                                        <select id="have_you_trained" name="have_you_trained" class="form-control">
                                            <option value="" selected disabled>Selecione</option>
                                            <option value="N"{{ old('have_you_trained') == 'N' ? ' selected':'' }}>Nunca</option>
                                            <option value="J"{{ old('have_you_trained') == 'J' ? ' selected':'' }}>Já treinei</option>
                                            <option value="T"{{ old('have_you_trained') == 'T' ? ' selected':'' }}>Treino</option>
                                        </select>
                                        @if ($errors->has('have_you_trained'))
                                            <span class="help-block">{{ $errors->first('have_you_trained') }}</span>
                                        @endif
                                    </div>

                                </div>

                                <div class="form-group detalhe-treino">

                                    <div class="col-md-7{{ $errors->has('team') ? ' has-error' : '' }}">
                                        <label for="team" class="control-label">Equipe?</label>
                                        <input type="text" id="team" name="team" value="{{ old('team') }}"
                                               class="form-control">
                                        @if ($errors->has('team'))
                                            <span class="help-block">{{ $errors->first('team') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-5{{ $errors->has('graduation') ? ' has-error' : '' }}">
                                        <label for="graduation" class="control-label">Graduação?</label>
                                        <select id="graduation" name="graduation" class="form-control">
                                            <option value="" selected disabled>Selecione</option>
                                            <option value="BR"{{ old('graduation' == 'BR' ? ' selected':'') }}>BRANCA</option>
                                            <option value="CI"{{ old('graduation' == 'CI' ? ' selected':'') }}>CINZA</option>
                                            <option value="AM"{{ old('graduation' == 'AM' ? ' selected':'') }}>AMARELA</option>
                                            <option value="LA"{{ old('graduation' == 'LA' ? ' selected':'') }}>LARANJA</option>
                                            <option value="VE"{{ old('graduation' == 'VE' ? ' selected':'') }}>VERDE</option>
                                            <option value="AZ"{{ old('graduation' == 'AZ' ? ' selected':'') }}>AZUL</option>
                                            <option value="RO"{{ old('graduation' == 'RO' ? ' selected':'') }}>ROXA</option>
                                            <option value="MA"{{ old('graduation' == 'MA' ? ' selected':'') }}>MARROM</option>
                                            <option value="PR"{{ old('graduation' == 'PR' ? ' selected':'') }}>PRETA</option>
                                        </select>
                                        @if ($errors->has('graduation'))
                                            <span class="help-block">{{ $errors->first('graduation') }}</span>
                                        @endif
                                    </div>

                                </div>

                                <div class="form-group">

                                    <div class="col-md-3{{ $errors->has('visit_date') ? ' has-error' : '' }}">
                                        <label for="visit_date" class="control-label">Data da visita</label>
                                        <input type="date" id="visit_date" name="visit_date"  value="{{ old('visit_date') }}"
                                               class="form-control datepicker">
                                        @if ($errors->has('visit_date'))
                                            <span class="help-block">{{ $errors->first('visit_date') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-3{{ $errors->has('visit_time') ? ' has-error' : '' }}">
                                        <label for="visit_time" class="control-label">Hora da visita</label>
                                        <select id="visit_time" name="visit_time" class="form-control">
                                            <option value="" disabled selected>Selecione</option>
                                            <option value="08:00"{{ old('visit_time') == '08:00' ? ' selected':'' }}>08:00</option>
                                            <option value="09:00"{{ old('visit_time') == '09:00' ? ' selected':'' }}>09:00</option>
                                            <option value="10:00"{{ old('visit_time') == '10:00' ? ' selected':'' }}>10:00</option>
                                            <option value="12:00"{{ old('visit_time') == '12:00' ? ' selected':'' }}>12:00</option>
                                            <option value="17:00"{{ old('visit_time') == '17:00' ? ' selected':'' }}>17:00 (Infantil)</option>
                                            <option value="19:00"{{ old('visit_time') == '19:00' ? ' selected':'' }}>19:00</option>
                                            <option value="21:00"{{ old('visit_time') == '21:00' ? ' selected':'' }}>21:00</option>
                                        </select>
                                        @if ($errors->has('visit_time'))
                                            <span class="help-block">{{ $errors->first('visit_time') }}</span>
                                        @endif
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col-md-4 col-md-offset-8">
                                        <button type="submit" class="btn btn-primary btn-block">Agendar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <script>
        $(document).ready(function(){
            $(".detalhe-treino").hide();

            $("#have_you_trained").on('change', function(){
                selected = $("#have_you_trained").val();

                selected == 'J' || selected == 'T' ? $(".detalhe-treino").fadeIn(500) : $(".detalhe-treino").fadeOut(500);
            });
        });
    </script>

@endsection
