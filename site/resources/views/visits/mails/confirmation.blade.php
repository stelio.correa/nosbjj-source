@component('mail::message')

    # Olá {{$visit->user->getFirstName()}}

    Sua visita foi agendada com sucesso.

    * Data: {{ $visit->visit_date }}
    * Horário: {{ $visit->visit_time }}

    Aguardamos sua presença.

    Obrigado,

    Nós BJJ
@endcomponent