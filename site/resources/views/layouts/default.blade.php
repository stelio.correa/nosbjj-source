<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/x-icon" href="{{ asset('/img/favicon.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>NÓS BJJ :: @yield('title')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    @yield('styles')

</head>
<body>
    <header class="parallax">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">NÓS BJJ</a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav"></ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="{{ route('visits.new') }}" title="Agendar visita">
                                <i class="fa fa-calendar"></i> Visita
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('matricula.download') }}" title="Baixar formulário de matrícula">
                                <i class="fa fa-download"></i> Matrícula
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        @yield('header-content')

    </header>

    <section class="container">
        @yield('content')
    </section>

    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-sm-6 footerleft ">
                    <h6 class="heading7">NÓS BJJ</h6>
                    <p>
                    <blockquote>
                        “Não basta ser Jiu-Jitsu, tem que ser Brasileiro”
                    </blockquote>
                    </p>
                    <p><i class="fa fa-map-pin"></i> Cidade Nova 9, Tv. WE 5B, 71B</p>
                    <p><i class="fa fa-phone"></i> 91 98485-0202</p>
                    <p><i class="fa fa-envelope"></i> E-mail : contato@nosbjj.com.br</p>

                </div>
                <div class="col-md-6 col-sm-6 paddingtop-bottom">
                    <h6 class="heading7">PARCEIROS</h6>
                    <ul class="list-inline">
                        <li><img src="{{ asset('/img/parceiros/Carpe_Vita.png') }}" alt="Carpe Vita" width="128"></li>
                        <li><img src="{{ asset('/img/parceiros/Clinica_Odonto_Dra_Nazare.png') }}" alt="Clínica Odontológica Dra. Nazaré Vieira" width="128"></li>
                        <li><img src="{{ asset('/img/parceiros/CNA_ingles.png') }}" alt="CNA" width="128"></li>
                        <li><img src="{{ asset('/img/parceiros/Daniel_Tattoo.png') }}" alt="Daniel Tattoo" width="128"></li>
                    </ul>
                    <ul class="list-inline">
                        <li><img src="{{ asset('/img/parceiros/Espaco_Dali.png') }}" alt="Espaço Dali" width="128"></li>
                        <li><img src="{{ asset('/img/parceiros/Fight_Arts.png') }}" alt="Fight Arts" width="128"></li>
                        <li><img src="{{ asset('/img/parceiros/Otica_Bela_Visao.png') }}" alt="Ótica Bela Visão" width="128"></li>
                        <li><img src="{{ asset('/img/parceiros/Sushi_Express.png') }}" alt="Sushi Express" width="128"></li>
                    </ul>
                    <ul class="list-inline">
                        <li><img src="{{ asset('/img/parceiros/WR_Info.png') }}" alt="WR Informática" width="128"></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-6 paddingtop-bottom">
                    <div class="fb-page" data-href="https://www.facebook.com/nosbjj"
                         data-tabs="timeline" data-height="300" data-small-header="false"
                         style="margin-bottom:15px;" data-adapt-container-width="true"
                         data-hide-cover="false" data-show-facepile="true">
                        <div class="fb-xfbml-parse-ignore">
                            <blockquote cite="https://www.facebook.com/nosbjj">
                                <a href="https://www.facebook.com/nosbjj" target="_blank">
                                    <i class="fa fa-facebook"></i> / NÓS BJJ
                                </a>
                            </blockquote>
                        </div>
                    </div>

                    <div class="fb-page" data-href="https://www.instagram.com/nos.bjj"
                         data-tabs="timeline" data-height="300" data-small-header="false"
                         style="margin-bottom:15px;" data-adapt-container-width="true"
                         data-hide-cover="false" data-show-facepile="true">
                        <div class="fb-xfbml-parse-ignore">
                            <blockquote cite="https://www.instagram.com/nos.bjj">
                                <a href="https://www.instagram.com/nos.bjj" target="_blank">
                                    <i class="fa fa-instagram"></i> / NÓS BJJ
                                </a>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="copyright">
        <div class="container">
            <div class="col-md-6">
                <p>© 2017 - NÓS BJJ</p>
            </div>
            <div class="col-md-6">
                <ul class="bottom_ul">
                    <li><a href="{{ url('/') }}">NÓS BJJ</a></li>
                    <li>
                        <a href="{{ route('visits.new') }}" title="Agendar visita">Visita</a>
                    </li>
                    <li>
                        <a href="{{ route('matricula.download') }}"
                           title="Baixar formulário de matrícula">Matrícula</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <script src="{{ mix('/js/app.js') }}" charset="UTF-8"></script>
    <script src="{{ asset('/js/jquery.mask.js') }}" charset="UTF-8"></script>
    <script src="{{ asset('/js/picker.js') }}" charset="utf-8"></script>
    <script src="{{ asset('/js/picker.date.js') }}" charset="utf-8"></script>
    <script src="{{ asset('/js/picker.time.js') }}" charset="utf-8"></script>
    <script src="{{ asset('/js/pickadate.js') }}" charset="utf-8"></script>
    @yield('scripts')
</body>
</html>
